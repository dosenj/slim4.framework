<?php

session_start();

use DI\Container;
use Slim\Factory\AppFactory;
use Illuminate\Database\Capsule\Manager as Capsule;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Slim\Flash\Messages;

require __DIR__ . '/../vendor/autoload.php';

$container = new Container();

$container->set('view', function($container){
	$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../core/Mvc/Views');

	$twig = new \Twig\Environment($loader, [
    	'cache' => false,
	]);

	return $twig;
});

$capsule = new Capsule();

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'slim4',
    'username'  => 'root',
    'password'  => 'aurora',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

$capsule->setAsGlobal();

$capsule->bootEloquent();

$container->set('db', function($container) use ($capsule){

	return $capsule;

});

$container->set('monolog', function($container){
	$log = new Logger('mon');
	$log->pushHandler(new StreamHandler(__DIR__ . '/../logs/app.log', Logger::INFO));
	return $log;
});

$container->set('flash', function($container){
	$flash = new Messages();
	return $flash;
});

$container->set('TestController', function($container){
	return new App\Mvc\Controllers\TestController($container);
});

$container->set('AuthController', function($container){
	return new App\Mvc\Controllers\AuthController($container);
});

$container->set('DevController', function($container){
	return new App\Mvc\Controllers\DevController($container);
});

$container->set('UserController', function($container){
	return new App\Mvc\Controllers\UserController($container);
});

AppFactory::setContainer($container);

$app = AppFactory::create();

$app->addRoutingMiddleware();

$app->addErrorMiddleware(true, true, true);

require __DIR__ . '/../routes/routes.php';