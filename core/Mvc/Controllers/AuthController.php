<?php

namespace App\Mvc\Controllers;

use App\Mvc\Models\User;

class AuthController extends Controller
{
	public function registerForm($request, $response)
	{
		$view = $this->container->get('view');
		echo $view->render('register.twig');
		return $response;
	}

	public function register($request, $response)
	{
		$name = $request->getParsedBody()['name'];
		$email = $request->getParsedBody()['email'];
		$password = $request->getParsedBody()['password'];

		// validation

		// insert record

		$user = User::create([
			'name' => $name,
			'email' => $email,
			'password' => password_hash($password, PASSWORD_DEFAULT)
		]);

		if($user){
			$logUserRegistred = $this->container->get('monolog');
			$logUserRegistred->info('New user registred successfully');

			$flashMessage = $this->container->get('flash');
			$flashMessage->addMessage('Test', 'Thanks for joining us');

			// redirect to homepage
			return $response->withHeader('Location', '/home')->withStatus(302);
		}

		// end of insert

	}

	public function loginForm($request, $response)
	{
		$view = $this->container->get('view');
		echo $view->render('login.twig');
		return $response;
	}

	public function login($request, $response)
	{
		$email = $request->getParsedBody()['email'];
		$password = $request->getParsedBody()['password'];

		// validation

		// check email and password match

		return $response;
	}

	public function home($request, $response)
	{

		$flashMess = $this->container->get('flash');

		$messages = $flashMess->getMessages();

		print_r($messages);

		$view = $this->container->get('view');
		echo $view->render('homepage.twig');
		return $response;
	}
}