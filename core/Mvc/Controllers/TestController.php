<?php

namespace App\Mvc\Controllers;

class TestController extends Controller
{
	public function test($request, $response)
	{
		$response->getBody()->write('works well');
		return $response;
	}

	public function testRenderingTemplate($request, $response)
	{
		$view = $this->container->get('view');
		echo $view->render('test.twig');
		return $response;
	}

	public function baz($request, $response)
	{
		$response->getBody()->write('some data to display');
		return $response;
	}

	public function testMiddleware($request, $response)
	{
		$response->getBody()->write('Some stuff');
		return $response;
	}
}