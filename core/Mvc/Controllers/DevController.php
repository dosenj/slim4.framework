<?php

namespace App\Mvc\Controllers;

class DevController extends Controller
{
	public function uploadForm($request, $response)
	{
		$view = $this->container->get('view');
		echo $view->render('upload.twig');
		return $response;
	}

	public function uploadData($request, $response)
	{
		$file = $request->getUploadedFiles();

		$uploadedFile = $file['file'];

		if($uploadedFile->getError() === UPLOAD_ERR_OK){
			
			$filename = $uploadedFile->getClientFileName();

			$extension = pathinfo($filename, PATHINFO_EXTENSION);

			$uploadedFile->moveTo('C:\xampp\htdocs\slim4.framework\public\uploads\\' . $filename);

		}

		return $response;
	}
}