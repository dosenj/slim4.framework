<?php

namespace App\Mvc\Controllers;

use App\Mvc\Models\User;

class UserController extends Controller
{
	public function showUsers($request, $response)
	{
		$users = User::all();

		$view = $this->container->get('view');

		echo $view->render('users.twig', ['users' => $users]);

		return $response;
	}

	public function getUser($request, $response, $args)
	{
		$id = $args['id'];

		$user = User::where('id', $id)->first();

		$view = $this->container->get('view');

		echo $view->render('user.twig', ['user' => $user]);

		return $response;
	}
}