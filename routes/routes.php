<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use App\Middleware\TestMiddleware;

$app->get('/testing', function(Request $request, Response $response){
	$response->getBody()->write('testing data ee');
	return $response;
});

$app->get('/foo', 'TestController:test');

$app->get('/bar', 'TestController:testRenderingTemplate');

$app->get('/baz', TestController::class . ':baz');

$app->get('/test/middleware', 'TestController:testMiddleware')->add(new TestMiddleware());

// Auth routes

$app->get('/register', 'AuthController:registerForm')->setName('register');
$app->post('/register', 'AuthController:register');

$app->get('/login', AuthController::class . ':loginForm')->setName('login');
$app->post('/login', AuthController::class . ':login');

$app->get('/home', 'AuthController:home')->setName('home');

// File upload routes

$app->get('/upload', 'DevController:uploadForm')->setName('upload.files');
$app->post('/upload', 'DevController:uploadData');

// User routes

$app->get('/users', 'UserController:showUsers')->setName('all.users');

$app->get('/users/{id}', 'UserController:getUser')->setName('user.details');